from rest_framework import serializers

from .models import Article, Category


class ArticleSerializer(serializers.ModelSerializer):
    category = serializers.ReadOnlyField(source="category.title")

    class Meta:
        model = Article
        fields = [
            "pk",
            "title",
            "content",
            "created_at",
            "updated_at",
            "photo",
            "is_published",
            "category",
        ]


class ArticleDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = [
            "pk",
            "title",
            "content",
            "created_at",
            "updated_at",
            "photo",
            "is_published",
            "category",
        ]


class CategorySerializer(serializers.ModelSerializer):
    articles = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ["pk", "title", "articles"]
