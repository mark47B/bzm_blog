from rest_framework import generics

from . import serializers
from .models import Article, Category


class ArticleList(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = serializers.ArticleSerializer

    def perform_create(self, serializer):
        serializer.save(category=self.request.Category)


class ArticleCreate(generics.CreateAPIView):
    serializer_class = serializers.ArticleDetailSerializer


class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class = serializers.ArticleSerializer


class CategoryList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer
