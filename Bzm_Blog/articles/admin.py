from django import forms
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Article, Category


class ArticleAdminForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = "__all__"


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    list_display = (
        "id",
        "title",
        "created_at",
        "updated_at",
        "is_published",
        "category",
        "get_photo",
    )
    list_display_links = ("id", "title")
    search_fields = ("title", "content")
    list_editable = ("is_published", "category")
    list_filter = ("is_published", "category")
    save_on_top = True
    fields = (
        "title",
        "category",
        "content",
        "photo",
        "get_photo",
        "is_published",
        "created_at",
        "updated_at",
    )
    readonly_fields = ("created_at", "get_photo", "updated_at")

    def get_photo(self, obj):
        if obj.photo:
            return mark_safe(f'<img src="{obj.photo.url}", width="50">')
        else:
            return "No photo"

    get_photo.short_description = "Миниатюра"


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("id", "title")
    list_display_links = ("id", "title")
    search_fields = ("title",)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)

admin.site.site_title = "BZM_Blog CRM"
admin.site.site_header = "BZM_Blog CRM"
